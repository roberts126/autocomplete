package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"sort"
	"time"
)

const (
	contentHeader = "Content-Type"
	contentType   = "application/json"
	keyMessage    = "message"
	srvrTimeout   = 30 * time.Second
)

// A WordCount contains a word and integer count. This is used to sort the results
type WordCount struct {
	// word is the string representation of the word being counted
	word string
	// count is the number of times a word is repeated
	count int
}

// startServer starts a web server (with or without TLS) and listens on the specified port
func startServer() {
	router := http.NewServeMux()
	router.HandleFunc("/", catchAll)
	router.HandleFunc("/ping", pingRequest)
	router.HandleFunc("/autocomplete", autocompleteRequest)

	webServer := &http.Server{
		Addr:         fmt.Sprintf(":%d", port),
		ReadTimeout:  srvrTimeout,
		WriteTimeout: srvrTimeout,
		Handler:      router,
	}

	go func() {
		if cert != "" && key != "" {
			log.Printf("starting server with TLS on port %d", port)
			if err := webServer.ListenAndServeTLS(cert, key); err != nil {
				log.Fatal(err)
			}
		} else {
			log.Printf("starting server on port %d", port)
			if err := webServer.ListenAndServe(); err != nil {
				log.Fatal(err)
			}
		}
	}()

	serverChan := make(chan os.Signal, 1)

	signal.Notify(serverChan, os.Interrupt)

	<-serverChan

	ctxDeadline := time.Now().Add(15 * time.Second)
	ctx, ctxCancel := context.WithDeadline(context.Background(), ctxDeadline)
	defer ctxCancel()

	_ = webServer.Shutdown(ctx)
	log.Println("shutting down")

	os.Exit(0)
}

// catchAll handles all non-specified routes and returns a 405
func catchAll(w http.ResponseWriter, _ *http.Request) {
	respondNotAllowed(w)
}

// pingRequest handles the ping request that can used as an uptime indicator
func pingRequest(w http.ResponseWriter, _ *http.Request) {
	respondWithMessage(w, http.StatusOK, "pong")
}

// autocompleteRequest handles the look up of a term compared to the data pile
func autocompleteRequest(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		respondNotAllowed(w)
		return
	}

	term := r.URL.Query().Get("term")
	if err := validateTerm(term); err != nil {
		respondBadRequest(w, err.Error())
		return
	}

	termRegex := fmt.Sprintf(`(?i)\b((?:[a-z'\-]*)%s(?:[a-z'\-]*))\b`, term)
	search, err := regexp.Compile(termRegex)
	if err != nil {
		log.Printf("error building term regex; regex: %s, err: %v\n", termRegex, err)
		respondServerError(w)
		return
	}

	matches := search.FindAllStringSubmatch(dataPile, -1)
	wordCounts := make([]WordCount, len(matches))
	for i, match := range matches {
		if len(match) > 1 {
			word := match[1]
			var count int
			var ok bool

			if count, ok = dictionary[word]; !ok {
				count = -1
			}

			wordCounts[i] = WordCount{
				word:  word,
				count: count,
			}
		}
	}

	sort.Slice(wordCounts, func(i, j int) bool {
		return wordCounts[i].count > wordCounts[j].count
	})

	if len(wordCounts) > 25 {
		wordCounts = wordCounts[:25]
	}

	matchedWords := make([]string, 0)
	for _, w := range wordCounts {
		matchedWords = append(matchedWords, w.word)
	}

	output := map[string]interface{}{
		"matches": matchedWords,
	}

	respondWithContent(w, http.StatusOK, output)
}

// validateTerm ensures the specified term meets the requirements of not empty and greater than one character
func validateTerm(term string) error {
	if term == "" {
		return errors.New("you must supply a term to match against")
	}

	if len(term) < 2 {
		return errors.New("term must be greater than one character")
	}

	return nil
}

// respondBadRequest responds with a 400 status code and a message
func respondBadRequest(w http.ResponseWriter, msg string) {
	respondWithMessage(w, http.StatusBadRequest, msg)
}

// respondBadRequest responds with a 405 status code and a message
func respondNotAllowed(w http.ResponseWriter) {
	respondWithMessage(w, http.StatusMethodNotAllowed, "method not allowed")
}

// respondBadRequest responds with a 500 status code and a message
func respondServerError(w http.ResponseWriter) {
	respondWithMessage(w, http.StatusInternalServerError, "internal server error")
}

// respondBadRequest responds with a status code and a simple map[string]interface{} containing a message property
func respondWithMessage(w http.ResponseWriter, code int, msg string) {
	var data map[string]interface{}

	data = map[string]interface{}{
		keyMessage: msg,
	}

	respondWithContent(w, code, data)
}

// respondBadRequest responds with a status code and an interface representing the structure of the response body
func respondWithContent(w http.ResponseWriter, code int, data interface{}) {
	w.Header().Add(contentHeader, contentType)
	w.WriteHeader(code)

	b, err := json.Marshal(data)

	if err != nil {
		log.Printf("error marshalling data for http response; err: %v\n", err)
	}

	_, _ = w.Write(b)
}
