package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"
)

const (
	minPort = 1000
	maxPort = 10000
)

var (
	// port is the numerical port the server listens on
	port int
	// dataSrc is the path to the actual data source text file
	dataSrc string
	// cert is the path to the TLS certificate
	cert string
	// key is the path to the TLS certificate key file
	key string
	// dictionary is a key/value store representing a instance of a word and count
	dictionary map[string]int
	// uniqueWords is a slice of strings representing a unique list of words
	uniqueWords []string
	// dataPile is a space delimited representation of all of the available words
	dataPile string
	// wordRegex is used to parse words from punctuation
	wordRegex *regexp.Regexp
	// lock prevents race conditions for dictionary writes
	lock sync.RWMutex
	// wg tracks updates to the dictionary
	wg sync.WaitGroup
)

func init() {
	dictionary = make(map[string]int, 0)
	uniqueWords = make([]string, 0)
	wordRegex = regexp.MustCompile(`[\W]`)

	flag.IntVar(&port, "port", 9000, "The port the web server will listen on")
	flag.StringVar(&dataSrc, "data", "./data/data-src.txt", "A text file containing the data used to match token")
	flag.StringVar(&cert, "cert", "", "The path to the certificate for TLS")
	flag.StringVar(&key, "jet", "", "The path to the certificate key file for TLS")

	flag.Parse()

	validateFlags()
}

func main() {
	loadText()
	startServer()
}

// addToDictionary adds a word to the dictionary, unique words, and keeps track of the count
func addToDictionary(w string) {
	lock.Lock()
	defer lock.Unlock()

	w = strings.ToLower(w)
	w = wordRegex.ReplaceAllString(w, "")

	if w == "" {
		return
	}

	if _, ok := dictionary[w]; !ok {
		dictionary[w] = 0
		uniqueWords = append(uniqueWords, w)
	}

	dictionary[w]++
}

// loadText reads in all of the text from the data source
func loadText() {
	f, err := os.Open(dataSrc)
	if err != nil {
		log.Fatalf("could not open data source file; error %v", err)
	}

	wg = sync.WaitGroup{}
	lock = sync.RWMutex{}

	defer func() {
		if err := f.Close(); err != nil {
			log.Println("could not close data source")
		}
	}()

	data, err := ioutil.ReadFile(dataSrc)
	if err != nil {
		log.Fatalf("could not read file; err: %v", err)
	}

	hyphenRegex := regexp.MustCompile(`(?i)([a-z])-[\r\n]+([a-z])`)
	data = hyphenRegex.ReplaceAll(data, []byte("$1$2"))

	newLineRegex := regexp.MustCompile(`[\r\n]+`)
	data = newLineRegex.ReplaceAll(data, []byte("\n"))

	lines := strings.Split(string(data), "\n")

	for i := range lines {
		wg.Add(1)

		go readLine(lines[i])
	}

	wg.Wait()
	dataPile = strings.Join(uniqueWords, " ")
	dataPile = " " + strings.Trim(dataPile, " ") + " "
	return
}

// readLine trims, splits, and send the sanitized words to addToDictionary
func readLine(line string) {
	defer wg.Done()
	lines := strings.Split(strings.Trim(line, " "), " ")

	for i := range lines {
		addToDictionary(lines[i])
	}

	return
}

// validateFlags validates the flag information to prevent errors
func validateFlags() {
	if port < minPort || port > maxPort {
		log.Fatalf("invalid port number: %d; port must be between %d and %d", port, minPort, maxPort)
	}

	if (cert != "" && key == "") || (key != "" && cert == "") {
		log.Fatalf("invalid TLS configuration; missing certificate or key file")
	}

	f, err := os.Stat(dataSrc)

	if os.IsNotExist(err) {
		log.Fatalf("data source file does not exist")
	}

	if f.IsDir() {
		log.Fatalf("data source must be a file; directory specified")
	}
}
