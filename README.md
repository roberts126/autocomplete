# Autocomplete API
Provides an endpoint to match words for autocomplete

### Getting Started

##### Building the web server

```
cd src
go build -o ../autocomplete
```

##### Starting the web server

Using the default `port (9000)`, `data source (./data/data-src.txt)`, and no TLS.
```
./autocomplete
```

Specifying the port
```
./autocomplete --port=8080
```

Specifying the path to the data source
```
./autocomplete --data=/home/ubuntu/autocomplete/data-src.txt
```

Specifying TLS
```
./autocomplete --cert=/etc/ssl/certs/autocomplete.crt --key=/etc/ssl/private/autocomplete.key
```

Kitchen Sink
```
./autocomplete  --port=8080  --data=/home/ubuntu/autocomplete/data-src.txt --cert=/etc/ssl/certs/autocomplete.crt --key=/etc/ssl/private/autocomplete.key
```

### Examples
```
curl http://localhost:9000/autocomplete?term=th
```

Response:
```
{
  "matches": [
    "the"
    "this"
    "...",
    "that"
  ]
}
```
